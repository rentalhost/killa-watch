var moment = require("moment"),
    cliColor = require("cli-color"),
    us = require("underscore"),

    colorTimeline = cliColor.blackBright;

exports.writeTimeline = function (message) {
    process.stdout.write("[" + colorTimeline(moment().format("HH:mm:ss")) + "] " + message);
};

exports.prioritizeArray = function (input, preorder) {
    var firstArray = us.intersection(preorder, input),
        secondArray = us.difference(input, preorder);

    return firstArray.concat(secondArray);
};
