var gulp = require("gulp"),

    // Gulp.
    gulpBrowserify = require("gulp-browserify"),
    gulpMinifyCSS = require("gulp-minify-css"),
    gulpUglify = require("gulp-uglify"),
    gulpRename = require("gulp-rename"),
    gulpConcat = require("gulp-concat"),
    gulpWatch = require("gulp-watch"),
    gulpLess = require("gulp-less"),
    gulpIf = require("gulp-if"),

    // Others.
    cliColor = require("cli-color"),
    extend = require("extend"),
    glob = require("glob"),

    // Specials.
    us = require("underscore"),
    argv = require("yargs").argv,

    // Node.
    path = require("path"),
    fs = require("fs"),

    // Classes.
    Stream = require("stream"),
    Q = require("q"),

    // Internal.
    utils = require("./utils.js"),

    // Colors.
    colorFilePath = cliColor.yellowBright.bold,
    colorFileEvent = cliColor.blueBright,
    colorStatusSucess = cliColor.greenBright,
    colorStatusFail = cliColor.redBright,
    colorMessageTopic = cliColor.blackBright,
    colorMessageCode = cliColor.magenta,

    // Variables.
    translateEvent = {
        "add": "added",
        "change": "changed",
        "unlink": "unlinked",
    };

function buildErrorDeferred(deferred) {
    return function (err) {
        process.stdout.write(colorStatusFail("Fail") + "\u0007\n");

        // Less, only.
        if (err.message) {
            var message = err.message.replace(/\sTried.+$/, "").replace(/'(.+?)'/, colorFilePath("$1")),
                filename = path.relative(process.cwd(), err.filename),
                content = fs.readFileSync(err.filename).toString().split(/\r?\n/)[err.line - 1].trim();

            process.stdout.write("\n           " + colorStatusFail.bold("Error") + "\n");
            process.stdout.write("           " + colorMessageTopic("Message: ") + message + "\n");
            process.stdout.write("           " + colorMessageTopic("File:    ") + filename);
            process.stdout.write(colorMessageTopic("::" + err.line) + "\n");
            process.stdout.write("           " + colorMessageTopic("Snippet: ") + colorMessageCode(content) + "\n\n");
        }

        deferred.resolve();
        return false;
    };
}

function buildSuccessDeferred(deferred) {
    return function () {
        process.stdout.write(colorStatusSucess("OK") + "\n");
        deferred.resolve();
    };
}

function prettyFile(file) {
    return colorFilePath(path.normalize(file));
}

function globPrioritize(gulpWatcher) {
    var files = [];

    // Collect all files on capture.
    us.each(transformsToArray(gulpWatcher.capture), function (filesPattern) {
        files = files.concat(glob.sync(filesPattern));
    });

    // Apply prioritize array.
    if (gulpWatcher.prioritize && gulpWatcher.prioritize.length) {
        files = utils.prioritizeArray(files, gulpWatcher.prioritize);
    }

    return files;
}

function minifyLESS(gulpWatcher) {
    var deferred = Q.defer();

    gulp.src(globPrioritize(gulpWatcher))
        .pipe(gulpLess())
        .on("error", buildErrorDeferred(deferred))
        .pipe(gulpIf(argv.production, gulpMinifyCSS()))
        .pipe(gulpRename(gulpWatcher.output))
        .pipe(gulp.dest(process.cwd()))
        .on("end", buildSuccessDeferred(deferred));

    return deferred.promise;
}

function minifyCSS(gulpWatcher) {
    var deferred = Q.defer();

    gulp.src(globPrioritize(gulpWatcher))
        .pipe(gulpConcat(gulpWatcher.output))
        .pipe(gulpIf(argv.production, gulpMinifyCSS()))
        .pipe(gulp.dest(process.cwd()))
        .on("end", buildSuccessDeferred(deferred));

    return deferred.promise;
}

function minifyJS(gulpWatcher) {
    var deferred = Q.defer();

    gulp.src(globPrioritize(gulpWatcher))
        .pipe(gulpIf(argv.browserify, gulpBrowserify({ "detectGlobals": false })))
        .pipe(gulpConcat(gulpWatcher.output))
        .pipe(gulpIf(argv.production, gulpUglify()))
        .on("error", buildErrorDeferred(deferred))
        .pipe(gulp.dest(process.cwd()))
        .on("end", buildSuccessDeferred(deferred));

    return deferred.promise;
}

function startBuilder(gulpWatcher, message) {
    var promise = Q();

    // Notify.
    promise = promise.then(utils.writeTimeline.bind(utils.writeTimeline, message));

    // Build file.
    if (gulpWatcher.type === "less") {
        // Build a LESS style.
        promise = promise.then(minifyLESS.bind(null, gulpWatcher));
    } else if (gulpWatcher.type === "css") {
        // Build a CSS style.
        promise = promise.then(minifyCSS.bind(null, gulpWatcher));
    } else if (gulpWatcher.type === "js") {
        // Build a JS.
        promise = promise.then(minifyJS.bind(null, gulpWatcher));
    }

    return promise;
}

function startWatch(gulpWatcher) {
    var promise = Q(),
        gulpWatchOptions = { "base": process.cwd() },
        gulpWatchers = transformsToArray(gulpWatcher.watch);

    us.each(gulpWatchers, function (watchPattern) {
        // Notify.
        promise = promise.then(function () {
                utils.writeTimeline("Watching files " + prettyFile(watchPattern) + " to " + prettyFile(gulpWatcher.output) + ".\n");
        });

        // Watch.
        gulpWatch(watchPattern, gulpWatchOptions, function (file) {
            var rebuildingMessage = "Rebuilding file " + prettyFile(gulpWatcher.output) +
                " because " + prettyFile(file.relative) + " was " + colorFileEvent(translateEvent[file.event]) + ": ";
            startBuilder(gulpWatcher, rebuildingMessage);
        });
    });

    return promise;
}

function transformsToArray(value) {
    if (!Array.isArray(value)) {
        return [ value ];
    }

    return value;
}

// Default options.
argv = extend({
    "file": path.normalize(process.cwd() + "/killa-watch.json"),
    "production": false
}, argv);

// Print file info.
process.stdout.write("\n");
utils.writeTimeline("Loading watchers from " + prettyFile(path.relative(process.cwd(), argv.file)) + ".\n");

// No file found.
if (!fs.existsSync(argv.file)) {
    utils.writeTimeline(colorStatusFail("Fail:") + " file not found.\n\n");
    return;
}

// Load watcher file.
var watcherFile = extend({ "version": "1.0.0" }, JSON.parse(fs.readFileSync(argv.file).toString()));

// No watchers.
if (!watcherFile.watchers || !watcherFile.watchers.length) {
    utils.writeTimeline(colorStatusFail("Fail:") + " none watcher defined on file.\n\n");
    return;
}

// Promise.
var promise = Q();

// First build process.
process.stdout.write("\n");
us.each(watcherFile.watchers, function (gulpWatcher) {
    var buildingMessage = "Building file " + prettyFile(gulpWatcher.output) + ": ";
    promise = promise.then(startBuilder.bind(null, gulpWatcher, buildingMessage));
});

// Start watchers.
promise = promise.then(process.stdout.write.bind(process.stdout, "\n"));
us.each(watcherFile.watchers, function (gulpWatcher) {
    promise = promise.then(startWatch.bind(null, gulpWatcher));
});

// Watcher separator.
promise = promise.then(process.stdout.write.bind(process.stdout, "\n"));
