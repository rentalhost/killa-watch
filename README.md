# killa-watch

Watch and compile JS, CSS and LESS files.

# How to use

First, do `npm install killa-watch -g` to install this package globally. Next step is create a `killa-watch.json` file where you will use it. Generally, on root of your project.

This file will have the watch rules. Copy that structure and so you fill as you want. You can specific how many watchers you want here. 

```js
{
    "watchers": [
        {
            "watch": "path/to/resources/**/*.less",
            "capture": "path/to/resources/master.less",
            "output": "path/to/publics/master.css",
            "type": "less",

            // Optional.
            "prioritize": [
                "path/to/resources/file1.less",
                "path/to/resources/file2.less",
                "path/to/resources/fileN.less"
            ]
        }
    ]
}
```

The **watch** (*string*, *string[]*) key define the watch rules. In this example, it'll watch for all `.less` files in `path/to/resources` directory. Any file added, unlinked or modified will make **killa-watch** rebuild.

The **capture** (*string*, *string[]*) key define what files will be rebuilded -- you can use glob patterns here too. Is important to know that the watched file will not be processed directly. In this example, if watcher get something it will process the *capture*'d files.

The **output** (*string*) key define the output filename. After process the *capture* file, this file will be created or updated with result content.

The **type** (*enum: js, css, less*) key define how process the **capture** file. 

Optionally, you can set the **prioritize** key (*string[]*). If you use glob pattern on **capture**, possibly you will get an array of files and maybe you need concat this files in specific order. For example: you need that *jquery.js* be processed first, and others files after it. So you specify the *jquery.js* path here.

## Starting watcher

Now the magic starts. Open terminal in a folder with the `killa-watch.json` and call `killa-watch`. On start it, all your watchers will be compiled automatically, then the watch will start. Any file added, unlinked or changed will fire the rebuild process automatically.

If you need that processed file be minified too, just use parameter `--production`. Not is recomended you use it in development mode because is heavy -- not toooo heavy, but it is more than normal.

**Example output**

```
Loading watchers from killa-watch.json.

Building file public/site.css: OK
Building file public/site.js: OK
Building file public/vendor.css: OK
Building file public/vendor.js: OK

Watching files res/site/**/*.less to public/site.css.
Watching files res/site/**/*.js to public/site.js.
Watching files res/vendor/**/*.css to public/vendor.css.
Watching files res/vendor/**/*.js to public/vendor.js.

Rebuilding file public/site.css because res/site/site.less was changed: OK
```
